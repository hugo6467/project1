module.exports = {
  purge: [
    './*.html',
    './src/**/*.html',
    './src/**/*.vue',
    './src/**/*.jsx',
    './src/**/*.ts'
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('daisyui')
  ],
  daisyui: {
    themes: [
      'light',
      'dark',
      {
        newTheme: { /* your theme name */
          primary: '#ffd300', /* Primary color */
          'primary-focus': '#c800de', /* Primary color - focused */
          'primary-content': '#ffffff', /* Foreground content color to use on primary color */

          secondary: '#0b9444', /* Secondary color */
          'secondary-focus': '#097536', /* Secondary color - focused */
          'secondary-content': '#ffffff', /* Foreground content color to use on secondary color */

          accent: '#78ff09', /* Accent color */
          'accent-focus': '#40bd02', /* Accent color - focused */
          'accent-content': '#ffffff', /* Foreground content color to use on accent color */

          neutral: '#ff9109', /* Neutral color */
          'neutral-focus': '#bd6902', /* Neutral color - focused */
          'neutral-content': '#ffffff', /* Foreground content color to use on neutral color */

          'base-100': '#ffffff', /* Base color of page, used for blank backgrounds */
          'base-200': '#f9fafb', /* Base color, a little darker */
          'base-300': '#d1d5db', /* Base color, even more darker */
          'base-content': '#1f2937', /* Foreground content color to use on base color */

          info: '#2094f3', /* Info */
          success: '#009485', /* Success */
          warning: '#ff9900', /* Warning */
          error: '#ff5724'
        }
      },
    ]
  }
}
